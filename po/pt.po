# Portuguese translation for gxml.
# Copyright (C) 2015 gxml's COPYRIGHT HOLDER
# This file is distributed under the same license as the gxml package.
# Pedro Albuquerque <palbuquerque73@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: gxml master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gxml&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2015-10-31 00:34+0000\n"
"PO-Revision-Date: 2015-10-31 07:20+0000\n"
"Last-Translator: <>\n"
"Language-Team: Portuguese <gnome_pt@yahoogroups.com>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Language: pt_PT\n"
"X-Source-Language: C\n"

#: ../gxml/Document.vala:150
msgid "Invalid file"
msgstr "Ficheiro inválido"

#: ../gxml/Enumeration.vala:88
msgid "value is invalid"
msgstr "valor é inválido"

#: ../gxml/Enumeration.vala:129
msgid "text can not been parsed to enumeration type:"
msgstr "texto não pode ser processado para tipo de enumeração:"

#: ../gxml/libxml-ChildNodeList.vala:147
#, c-format
msgid "ref_child '%s' not found, was supposed to have '%s' inserted before it."
msgstr ""
"ref_child \"%s\" não encontrado, era suposto ter \"%s\" inserido antes."

#: ../gxml/libxml-Document.vala:137
#, c-format
msgid "Looking up %s from an xmlNode* is not supported"
msgstr "Procurar em %s a partir de um xmlNode* não é suportado"

#: ../gxml/libxml-Document.vala:311
#, c-format
msgid "Could not load document from path: %s"
msgstr "Impossível carregar o documento do caminho: %s"

#: ../gxml/libxml-Entity.vala:129
msgid "Cloning of Entity not yet supported"
msgstr "Clonagem de entidade ainda não é suportado"

#. TODO: replace usage of this with GXml.get_last_error_msg
#: ../gxml/libxml-Error.vala:12
#, c-format
msgid "%s:%s:%d: %s:%d: %s"
msgstr "%s:%s:%d: %s:%d: %s"

#: ../gxml/libxml-Node.vala:67
msgid "Node tried to interact with different document it belongs to"
msgstr "O nó tentou interagir com diferentes documentos a que pertence"

#: ../gxml/Node.vala:134
msgid "Text node with NULL string"
msgstr "Nó de texto com cadeia NULL"

#: ../gxml/SerializableEnum.vala:72
msgid "Value can't be parsed to a valid enumeration's value. Value is not set"
msgstr ""
"Valor não pode ser processado para valor de enumeração válido. Valor não "
"definido"

#: ../gxml/SerializableEnum.vala:75
msgid "Value can't be parsed to a valid enumeration's value"
msgstr "Valor não pode ser processado para valor de enumeração válido"

#: ../gxml/SerializableGeeArrayList.vala:139
#: ../gxml/SerializableGeeDualKeyMap.vala:224
#: ../gxml/SerializableGeeHashMap.vala:137
#: ../gxml/SerializableGeeTreeMap.vala:140
#, c-format
msgid "%s: Value type '%s' is unsupported"
msgstr "%s: tipo de valor %s não é suportado"

#: ../gxml/SerializableObjectModel.vala:173
msgid "Text node with NULL or none text"
msgstr "Nó de texto com NULL ou sem texto"

#: ../gxml/SerializableObjectModel.vala:297
#, c-format
msgid "WARNING: Object type '%s' have no Node Name defined"
msgstr "AVISO: tipo de objeto \"%s\" não tem Nome do nó definido"

#: ../gxml/SerializableObjectModel.vala:301
#, c-format
msgid "Actual node's name is '%s' expected '%s'"
msgstr "O nome atual do nó é \"%s\". Esperado \"%s\""

#: ../gxml/SerializableProperty.vala:93
msgid "Trying to serialize to a non GXmlElement!"
msgstr "A tentar serializar para um não GXmlElement!"

#: ../gxml/SerializableProperty.vala:112
msgid "No attribute found to deserialize from"
msgstr "Sem atributo para desserializar"

#: ../gxml/Serializable.vala:573
#, c-format
msgid "Transformation Error on '%s' or Unsupported type: '%s'"
msgstr "Erro de transformação em \"%s\" ou tipo não suportado: \"%s\""

#: ../gxml/Serializable.vala:615
#, c-format
msgid "Can't transform '%s' to string"
msgstr "Impossível transformar \"%s\" em cadeia"

#: ../gxml/Serialization.vala:184
#, c-format
msgid "DEBUG: skipping gpointer with name '%s' of object '%s'"
msgstr "DEPURAÇÃO: a saltar gpointer com nome \"%s\" do objeto \"%s\""

#: ../gxml/Serialization.vala:187
#, c-format
msgid "Can't currently serialize type '%s' for property '%s' of object '%s'"
msgstr ""
"Atualmente é impossível serializar o tipo \"%s\" para a propriedade \"%s\" "
"do objeto \"%s\""

#: ../gxml/Serialization.vala:403
#, c-format
msgid "Deserializing unknown GType '%s' objects is unsupported"
msgstr ""
"A desserialização de objetos GType \"%s\" desconhecidos não é suportada"

#: ../gxml/Serialization.vala:433
#, c-format
msgid "Unknown property '%s' found, for object type '%s'-->XML: [%s]"
msgstr ""
"Propriedade \"%s\" desconhecida encontrada, para tipo de objeto \"%s\" --> "
"XML: [%s]"
